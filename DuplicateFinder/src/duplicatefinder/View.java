package duplicatefinder;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.table.DefaultTableModel;

public class View extends JFrame {
	private BusinessLogic logic;

	public View() {
		ResourceBundle bundle = ResourceBundle.getBundle(Constants.ENGLISH_LANGUAGE_PATH);

		setTitle(Constants.SOFTWARE_TITLE + Constants.SOFTWARE_VERSION);
		setSize(640, 480);// 400 width and 500 height
		setVisible(true);// making the frame visible
		logic = new BusinessLogic();
		logic.setModel(new Model());
		getContentPane().setLayout(new BorderLayout());

		String[] columnNames = { "", bundle.getString("fileName"), bundle.getString("path"), bundle.getString("size"),
				bundle.getString("CRC") };
		DefaultTableModel dtm = new DefaultTableModel(null, columnNames);

		JTable table = new JTable(dtm) {
			@Override
			public Class<?> getColumnClass(int column) {
				if (column == 0) {
					return Boolean.class;
				}
				return String.class;
			}
		};
		logic.addToComponentList(table);
		JScrollPane scrollPane = new JScrollPane(table);
		getContentPane().add(scrollPane, BorderLayout.CENTER);

		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.SOUTH);

		// JLabel lblProva = new JLabel("prova");
		// lblProva.setHorizontalAlignment(SwingConstants.LEFT);
		// panel.add(lblProva);

		JPanel panel_1 = new JPanel();
		getContentPane().add(panel_1, BorderLayout.NORTH);
		panel_1.setLayout(new BorderLayout(0, 0));

		JMenuBar menuBar = new JMenuBar();
		panel_1.add(menuBar, BorderLayout.NORTH);
		JMenu file = new JMenu(bundle.getString(Constants.COMPONENT_WITH_NAME_FILE));
		file.setName(Constants.COMPONENT_WITH_NAME_FILE);
		menuBar.add(file);
		logic.addToComponentList(file);

		JMenuItem menuItem = new JMenuItem(bundle.getString(Constants.COMPONENT_WITH_NAME_OPEN_FOLDER));
		menuItem.setName(Constants.COMPONENT_WITH_NAME_OPEN_FOLDER);
		menuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser j = new JFileChooser();
				j.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				j.showOpenDialog(panel);
				if (j.getSelectedFile() != null && j.getSelectedFile().getAbsolutePath() != null) {
					logic.getArrayOfRows(j.getSelectedFile(), dtm);
				}
			}
		});
		file.add(menuItem);
		logic.addToComponentList(menuItem);

		JMenuItem exit = new JMenuItem(bundle.getString(Constants.COMPONENT_WITH_NAME_OPEN_EXIT));
		exit.setName(Constants.COMPONENT_WITH_NAME_OPEN_EXIT);
		exit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		file.add(exit);
		logic.addToComponentList(exit);

		JMenu mnSettings = new JMenu(bundle.getString(Constants.COMPONENT_WITH_NAME_OPEN_SETTINGS));
		mnSettings.setName(Constants.COMPONENT_WITH_NAME_OPEN_SETTINGS);
		logic.addToComponentList(mnSettings);
		menuBar.add(mnSettings);

		JMenu mnChangeLanguage = new JMenu(bundle.getString(Constants.COMPONENT_WITH_NAME_CHANGE_LANGUAGE));
		mnChangeLanguage.setName(Constants.COMPONENT_WITH_NAME_CHANGE_LANGUAGE);
		mnSettings.add(mnChangeLanguage);
		logic.addToComponentList(mnChangeLanguage);

		JMenuItem mntmItalian = new JMenuItem(bundle.getString(Constants.COMPONENT_WITH_NAME_ITALIAN));
		mntmItalian.setIcon(new ImageIcon(View.class.getResource(Constants.ITALIAN_FLAG_PATH)));
		mntmItalian.setName(Constants.COMPONENT_WITH_NAME_ITALIAN);
		logic.addToComponentList(mntmItalian);
		mnChangeLanguage.add(mntmItalian);
		mntmItalian.addActionListener(new LanguageListener(Constants.ITALIAN_LANGUAGE_PATH, this));

		JMenuItem mntmEnglish = new JMenuItem(bundle.getString(Constants.COMPONENT_WITH_NAME_ENGLISH));
		mntmEnglish.setIcon(new ImageIcon(View.class.getResource(Constants.ENGLISH_FLAG_PATH)));
		mntmEnglish.setName(Constants.COMPONENT_WITH_NAME_ENGLISH);
		mnChangeLanguage.add(mntmEnglish);
		logic.addToComponentList(mntmEnglish);
		mntmEnglish.addActionListener(new LanguageListener(Constants.ENGLISH_LANGUAGE_PATH, this));

		JToolBar toolBar = new JToolBar();
		panel_1.add(toolBar, BorderLayout.SOUTH);
		toolBar.setFloatable(false);

		JButton deleteButton = new JButton(bundle.getString(Constants.COMPONENT_WITH_NAME_DELETE_SELECTED));
		deleteButton.setName(Constants.COMPONENT_WITH_NAME_DELETE_SELECTED);
		logic.addToComponentList(deleteButton);
		deleteButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int dialogResult = JOptionPane.showConfirmDialog(null, bundle.getString("deleteMessage"),
						bundle.getString("warning"), JOptionPane.YES_NO_OPTION);
				if (dialogResult == JOptionPane.YES_OPTION) {
					for (int i = 0; i < dtm.getRowCount(); i++) {
						if ((boolean) dtm.getValueAt(i, 0)) {
							File f = new File((String) dtm.getValueAt(i, 2));
							f.delete();
							dtm.removeRow(i);
						}
					}
				}
			}
		});
		toolBar.add(deleteButton);

	}

	public BusinessLogic getLogic() {
		return logic;
	}

	public void setLogic(BusinessLogic logic) {
		this.logic = logic;
	}
}

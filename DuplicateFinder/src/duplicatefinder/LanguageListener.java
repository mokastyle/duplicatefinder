package duplicatefinder;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LanguageListener implements ActionListener {

	private String selectedLanguage;
	private View view;

	public LanguageListener(String language, View component) {
		super();
		this.selectedLanguage = language;
		this.view = component;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		view.getLogic().updateTextNameForComponents(selectedLanguage);
	}

	public String getSelectedLanguage() {
		return selectedLanguage;
	}

	public void setSelectedLanguage(String selectedLanguage) {
		this.selectedLanguage = selectedLanguage;
	}

	public View getView() {
		return view;
	}

	public void setView(View view) {
		this.view = view;
	}

}

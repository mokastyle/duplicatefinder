package duplicatefinder;

import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.zip.CRC32;

import javax.swing.AbstractButton;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

public class BusinessLogic {

	private Model model;

	public void getArrayOfRows(final File folder, DefaultTableModel dtm) {
		model.getDuplicates().clear();
		listFilesForFolder(folder);

		Iterator<DuplicateFile> iterator = model.getDuplicates().iterator();
		while (iterator.hasNext()) {
			DuplicateFile duplicate = iterator.next();
			if (duplicate.getDuplicatedFileGroup().size() < 2) {
				iterator.remove();
			} else {
				boolean check = false;
				for (File f : duplicate.getDuplicatedFileGroup()) {
					dtm.addRow(
							new Object[] { check, f.getName(), f.getAbsolutePath(), f.length(), duplicate.getCRC() });
					check = true;
				}
			}
		}
	}

	public void listFilesForFolder(final File folder) {
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				listFilesForFolder(fileEntry);
			} else {
				String CRC = calculateCheckSum(fileEntry);
				if (CRC != null) {
					addFileToDuplicates(CRC, fileEntry);
				}
			}
		}
	}

	public String calculateCheckSum(File fileEntry) {
		CRC32 crc = new CRC32();
		try {
			crc.update(Files.readAllBytes(Paths.get(fileEntry.getAbsolutePath())));
			System.out.println(Long.toHexString(crc.getValue()));
			return Long.toHexString(crc.getValue());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void addFileToDuplicates(String CRC, File fileEntry) {
		boolean found = false;
		for (DuplicateFile duplicateGroup : model.getDuplicates()) {
			if (duplicateGroup.getCRC().equalsIgnoreCase(CRC)) {
				duplicateGroup.getDuplicatedFileGroup().add(fileEntry);
				found = true;
				break;
			}
		}
		if (!found) {
			DuplicateFile newGroup = new DuplicateFile();
			newGroup.setCRC(CRC);
			newGroup.setDuplicatedFileGroup(new ArrayList<File>());
			newGroup.getDuplicatedFileGroup().add(fileEntry);
			model.getDuplicates().add(newGroup);
		}
	}

	public void addToComponentList(Component c) {
		model.getComponentList().add(c);
	}

	public void updateTextNameForComponents(String language) {
		ResourceBundle.clearCache();
		ResourceBundle bundle = ResourceBundle.getBundle(language, new Locale(""));
		for (Component c : model.getComponentList()) {
			if (c instanceof JLabel) {
				((JLabel) c).setText(bundle.getString(c.getName()));
			} else if (c instanceof AbstractButton) {
				((AbstractButton) c).setText(bundle.getString(c.getName()));
			} else if (c instanceof JTable) {
				JTableHeader header = ((JTable) c).getTableHeader();
				header.getColumnModel().getColumn(1).setHeaderValue(bundle.getString("fileName"));
				header.getColumnModel().getColumn(2).setHeaderValue(bundle.getString("path"));
				header.getColumnModel().getColumn(3).setHeaderValue(bundle.getString("size"));
				header.getColumnModel().getColumn(4).setHeaderValue(bundle.getString("CRC"));
				header.repaint();
			}
		}
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}
}

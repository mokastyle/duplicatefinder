package duplicatefinder;

public class Constants {
	public static String SOFTWARE_VERSION = "0.2";
	public static String SOFTWARE_TITLE = "Duplicate Finder v";
	public static String ENGLISH_LANGUAGE_PATH = "duplicatefinder.resources.messages";
	public static String ITALIAN_LANGUAGE_PATH = "duplicatefinder.resources.messages_it";
	public static String ITALIAN_FLAG_PATH = "/duplicatefinder/resources/IT.png";
	public static String ENGLISH_FLAG_PATH = "/duplicatefinder/resources/US.png";

	// COMPONENT NAMES
	public static String COMPONENT_WITH_NAME_FILE = "file";
	public static String COMPONENT_WITH_NAME_OPEN_FOLDER = "openFolder";
	public static String COMPONENT_WITH_NAME_OPEN_EXIT = "exit";
	public static String COMPONENT_WITH_NAME_OPEN_SETTINGS = "settings";
	public static String COMPONENT_WITH_NAME_CHANGE_LANGUAGE = "languageChange";
	public static String COMPONENT_WITH_NAME_ITALIAN = "italian";
	public static String COMPONENT_WITH_NAME_ENGLISH = "english";
	public static String COMPONENT_WITH_NAME_DELETE_SELECTED = "deleteSelected";

}

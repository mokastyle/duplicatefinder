package duplicatefinder;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

public class Model {
	private List<DuplicateFile> duplicates;
	private List<Component> componentList;

	public Model() {
		duplicates = new ArrayList<DuplicateFile>();
		componentList = new ArrayList<Component>();
	}

	public List<DuplicateFile> getDuplicates() {
		return duplicates;
	}

	public void setDuplicates(List<DuplicateFile> duplicates) {
		this.duplicates = duplicates;
	}

	public List<Component> getComponentList() {
		return componentList;
	}

	public void setComponentList(List<Component> componentList) {
		this.componentList = componentList;
	}

}

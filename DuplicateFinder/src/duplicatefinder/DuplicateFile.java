package duplicatefinder;

import java.io.File;
import java.util.List;

public class DuplicateFile {

	private List<File> duplicatedFileGroup;
	private String CRC;

	public List<File> getDuplicatedFileGroup() {
		return duplicatedFileGroup;
	}

	public void setDuplicatedFileGroup(List<File> duplicatedFileGroup) {
		this.duplicatedFileGroup = duplicatedFileGroup;
	}

	public String getCRC() {
		return CRC;
	}

	public void setCRC(String cRC) {
		CRC = cRC;
	}
}
